# awdb-C\#
www.ipplus360.com 官方支持的解析awdb格式的C#代码(Official support for parsing C# code in AWDB format )

### 1.安装C#环境 （dotnet_sdk >= 6.0）

**注:CentOS需要7以上的系统，ubuntu需要64位**

CentOS安装

**参考连接：https://learn.microsoft.com/zh-cn/dotnet/core/install/linux-centos**

ubuntu安装

**参考连接：https://learn.microsoft.com/zh-cn/dotnet/core/install/linux-ubuntu**


### 2.C#解析样例

```C#
dotnet run Program.cs
```

 

